package org.cent.starter.custom.prop;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/1/4.
 * @description: 自定义Starter配置类
 */
@ConfigurationProperties(prefix = "org.cent.stater.custom")
@Setter
@Getter
public class CustomProp {

    private String name = "cent";

    @NestedConfigurationProperty
    private String description = "天气真好";
}
