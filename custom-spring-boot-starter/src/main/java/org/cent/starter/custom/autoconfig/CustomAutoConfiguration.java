package org.cent.starter.custom.autoconfig;

import org.cent.starter.custom.prop.CustomProp;
import org.cent.starter.custom.service.CustomService;
import org.cent.starter.custom.service.impl.CustomServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/1/4.
 * @description: 自动配置类
 */
@Configuration
@EnableConfigurationProperties(CustomProp.class)
public class CustomAutoConfiguration {

    /**
     * 初始化自定义Starter的Bean
     *
     * @param customProp
     * @return
     */
    @Bean
    public CustomService customService(CustomProp customProp) {
        return new CustomServiceImpl(customProp);
    }
}
