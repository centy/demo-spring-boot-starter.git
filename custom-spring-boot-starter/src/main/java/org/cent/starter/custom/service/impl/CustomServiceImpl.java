package org.cent.starter.custom.service.impl;

import lombok.AllArgsConstructor;
import org.cent.starter.custom.prop.CustomProp;
import org.cent.starter.custom.service.CustomService;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/1/4.
 * @description: 示例业务接口实现类
 */
@AllArgsConstructor
public class CustomServiceImpl implements CustomService {

    private CustomProp customProp;

    @Override
    public void sayHello() {
        String message = String.format("你好！%s，%s!", customProp.getName(), customProp.getDescription());
        System.out.println(message);
    }
}
