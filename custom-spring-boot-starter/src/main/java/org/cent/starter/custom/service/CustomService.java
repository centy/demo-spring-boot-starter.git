package org.cent.starter.custom.service;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/1/4.
 * @description: 示例业务接口
 */
public interface CustomService {

    /**
     * 示例方法
     */
    void sayHello();
}
