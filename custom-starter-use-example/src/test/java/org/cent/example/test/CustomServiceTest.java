package org.cent.example.test;

import org.cent.custom.starter.example.Application;
import org.cent.starter.custom.service.CustomService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/1/4.
 * @description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
public class CustomServiceTest {

    @Autowired
    private CustomService customService;

    @Test
    public void sayHello(){
        customService.sayHello();
    }
}
