package org.cent.custom.starter.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: cent
 * @email: chenzhao@viomi.com.cn
 * @date: 2019/1/4.
 * @description: 示例启动类
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
